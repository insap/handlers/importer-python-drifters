import os
import shutil
import json


class Storage:

    @staticmethod
    def read_data_from_json_file(path: str):
        try:
            with open(path, encoding='utf-8') as json_file:
                return json.load(json_file)
        except EnvironmentError:
            return []

    @staticmethod
    def remove_all_files_from_path(path: str, re_create_path: bool = True):
        if os.path.exists(path):
            shutil.rmtree(path)

        if re_create_path:
            return os.mkdir(path)

        return True

    @staticmethod
    def write_json_to_file(path: str, data: list):
        with open(path, 'w', encoding='utf-8') as file:
            json.dump(data, file, ensure_ascii=False, indent=4)

    @staticmethod
    def copy_file(src: str, dest: str):
        shutil.copyfile(src, dest)
