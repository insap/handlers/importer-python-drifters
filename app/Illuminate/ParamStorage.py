import os.path


class ParamStorage:
    def __init__(self, files_path: str, params: dict, files: list, data: list = None):
        if data is None:
            data = list
        self.params = params
        self.files = files
        self.data = data
        self.files_path = files_path

    def get_file_by_type(self, file_type: str) -> str:
        for file in self.files:
            if file['type'] == file_type:
                return os.path.join(self.files_path, file['name'])

        return ""

    def get_param_by_name(self, param_name):
        return self.params[param_name]

    def get_params(self):
        return self.params

    def get_files(self):
        return self.files

    def get_data(self):
        return self.data

