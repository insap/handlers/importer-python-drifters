from openpyxl import load_workbook
import math
from datetime import datetime
from datetime import time

from app.Illuminate.ParamStorage import ParamStorage


# Функция подсчета расстояния через гаверсинус
def function_distance(lat1, long1, lat2, long2):
    EARTH_RADIUS = 6372795
    distance_result = 2 * EARTH_RADIUS * math.asin(math.sqrt(
        math.sin((lat2 - lat1) / 2) ** 2 + math.cos(lat1) * math.cos(lat2) * math.sin((long2 - long1) / 2) ** 2))
    return distance_result


def get_formatted_time(time_new):
    if type(time_new) is not time:
        time_new = datetime.strptime(str(time_new), "%H:%M:%S").time()

    time_new = str(time_new).split('.')[0] if "." in str(time_new) else time_new

    return time_new


def process(storage: ParamStorage) -> list:
    result = []

    wb = load_workbook(filename=storage.get_file_by_type('data'))  # Первичные данные
    sheetName = wb.sheetnames[0]
    sheet_ranges = wb[sheetName]

    row_count = sheet_ranges.max_row  # кол-во строк
    column_count = sheet_ranges.max_column  # кол-во столбцов
    end_str = 0

    for i in range(1, row_count + 1):
        if sheet_ranges.cell(row=i, column=1).value is not None:
            end_str = i
    row_count = end_str

    str_numb = 1  # Номер строки (измерения)

    date_new = sheet_ranges.cell(row=2, column=1).value
    time_new = get_formatted_time(sheet_ranges.cell(row=2, column=2).value)

    date_format = date_new.date().strftime("%Y-%m-%d")
    time_format = time_new.strftime("%H:%M:%S")

    first_row = dict({
        'step_id': str_numb,
        'date_time': datetime.strptime(date_format + " " + time_format, "%Y-%m-%d %H:%M:%S").strftime(
            "%Y-%m-%d %H:%M:%S"),
        'distance': 0,
        'speed': 0,
        'latitude': float(sheet_ranges.cell(row=2, column=3).value),
        'longitude': float(sheet_ranges.cell(row=2, column=4).value),
        'drifter_number': storage.get_param_by_name('drifter_number'),
        'utc_diff': storage.get_param_by_name('utc_diff'),
    })

    str_numb += 1
    result.append(first_row)

    for i in range(3, row_count + 1):
        lat1 = sheet_ranges.cell(row=i - 1, column=3).value * math.pi / 180  # Широта предыдущего измерения
        long1 = sheet_ranges.cell(row=i - 1, column=4).value * math.pi / 180  # Долгота предыдущего измерения
        lat2 = sheet_ranges.cell(row=i, column=3).value * math.pi / 180  # Широта текущего измерения
        long2 = sheet_ranges.cell(row=i, column=4).value * math.pi / 180  # Долгота текущего измерения

        distance = function_distance(lat1, long1, lat2, long2)  # Подсчет расстояния в метрах

        date_old = sheet_ranges.cell(row=i - 1, column=1).value
        time_old = get_formatted_time(sheet_ranges.cell(row=i - 1, column=2).value)
        date_new = sheet_ranges.cell(row=i, column=1).value
        time_new = get_formatted_time(sheet_ranges.cell(row=i, column=2).value)

        if type(date_old) is not datetime or type(date_new) is not datetime:
            print("В столбце дата не содержится значений формата Datetime")

        if type(time_old) is not time:
            time_old = datetime.strptime(str(time_old), "%H:%M:%S").time()

        time1 = datetime.combine(date_old, time_old)  # Комбинация data и time предыдущего измерения
        time2 = datetime.combine(date_new, time_new)  # Комбинация data и time текущего измерения
        dt_time = (time2 - time1).total_seconds()  # Разница в секундах
        speed = round(distance * 100 / dt_time, 2)  # Скорость, см/c

        # Состав списка:
        # Номер измерения, Дата, Время, Расстояние (в м), Скорость (в см/с), Широта, Долгота

        date_format = date_new.date().strftime("%Y-%m-%d")
        time_format = time_new.strftime("%H:%M:%S")

        res = dict({
            'step_id': str_numb,
            'date_time': datetime.strptime(date_format + " " + time_format, "%Y-%m-%d %H:%M:%S").strftime(
                "%Y-%m-%d %H:%M:%S"),
            'distance': float(round(distance, 3)),
            'speed': float(speed),
            'latitude': float(sheet_ranges.cell(row=i, column=3).value),
            'longitude': float(sheet_ranges.cell(row=i, column=4).value),
            'drifter_number': storage.get_param_by_name('drifter_number'),
            'utc_diff': storage.get_param_by_name('utc_diff'),
        })

        result.append(res)
        str_numb += 1

    wb.close()
    return result
